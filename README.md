# Welcome to **[pm-validator](https://bitbucket.com/sztadii/pm-validator)**!

## Why

When many developers are working on the same project each of them can use the different package manager ( npm or yarn ),
but using multi-lock files can make some small issues. 
To prevent it I created this package.

## Requirements

Available `npx` ( execute npm package binaries - default tool in latest npm )

### Run it with preinstall hook. We support only `npm` and `yarn` ( by default we use `npm` )

Example for yarn
``` 
"scripts": {
  "preinstall": "REQUIRED=yarn npx pm-validator"
  ...
}
```

Example for npm
``` 
"scripts": {
  "preinstall": "npx pm-validator"
  ...
}
```
